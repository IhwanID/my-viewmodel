package id.ihwan.myviewmodel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.ObservableField

class MainViewModel(application: Application) : AndroidViewModel(application) {

    var nama: ObservableField<String> = ObservableField()
    var kelas: ObservableField<String> = ObservableField()
    var jurusan: ObservableField<String> = ObservableField()

    fun setData(student: Student){
        nama.set(student.nama)
        kelas.set(student.kelas)
        jurusan.set(student.jurusan)
    }

}